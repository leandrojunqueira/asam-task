import React, { useState } from "react";
import cartIcon from "assets/images/cart.svg";
import Wishlist from "../wishlist/Wishlist";
import heart from "assets/images/wishlist.svg";

const Cart = (props) => {
  let {
    cart,
    setCart,
    setFavorite,
    favorite,
    removeFromFavorite,
    addToFavorite,
  } = props;

  const [sideBar, setSideBar] = useState([false]);

  //Open sidebar
  const sidebarOpenClose = () => {
    setSideBar((set) => !set);
  };

  //Close sidebar
  const closeSideBar = () => {
    if (sideBar) {
      setSideBar(false);
    }
  };

  //count products
  const products = cart.map((i) => i.id).length;

  //count favorite
  const fav = favorite.map((i) => i.id).length;

  //Remove products from cart
  const removeFromCart = (i) => {
    let hardCopy = [...cart];
    hardCopy = hardCopy.filter((cartItem) => cartItem.id !== i.id);
    setCart(hardCopy);
  };

  //Map Products added to cart
  const productsCart = cart.map((i) => (
    <div key={i.id} className="">
      <div className="simple-Cart">
        <ul className="cart-list ">
          <li className="listItem">
            <div className="cartItem">
              <div className="cartItem-image">
                <img
                  src={i.productImage.retina}
                  alt={i.name}
                  className="item-image"
                />
              </div>
              <div className="itemInfo">
                <span className="brand itemName">{i.cosBrand}</span>

                <span className="itemName">{i.name}</span>
                <div className="itemDescription">
                  {i.cosContent} -<span> {i.normPrice}</span>
                </div>
                <div className="itemDescription">{i.shortDescription}</div>
                <div className="itemAction">
                  <span
                    className="Link LinkPurple"
                    aria-hidden="true"
                    onClick={() => removeFromCart(i)}
                  >
                    Entfernen
                  </span>
                  <span className="Divider DividerVertical"></span>
                </div>
              </div>
              <div className="qtd">
                <p>Anzahl</p>
                <div style={{ position: "relative", opacity: "1" }}>
                  <div className="qtdNumber">1</div>
                </div>
              </div>
              <p className="itemPrice textPrice">{i.price} €</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  ));

  return (
    <div>
      <div className="iconRight" onClick={() => sidebarOpenClose()}>
        <span className="indicator"></span>
        <span className="iconMenu">
          <span className="qtdCart">{products}</span>
          <img className="cart" src={cartIcon} alt="cart" />
        </span>
        <span className="linkMenu">Warenkorb</span>

        <Sidebar
          cart={cart}
          sideBar={sideBar}
          handleSidebar={sidebarOpenClose}
          productsCart={productsCart}
          closeSideBar={closeSideBar}
          favorite={favorite}
          setFavorite={setFavorite}
          addToFavorite={addToFavorite}
          removeFromFavorite={removeFromFavorite}
        />
      </div>
      <div className="iconRight" onClick={() => sidebarOpenClose()}>
        <span className="indicator"></span>
        <span className="iconMenu">
          <span className="qtdCart">{fav}</span>

          <img className="wishlist" src={heart} alt="Wishlist" />
        </span>
        <span className="linkMenu">Wunschzettel</span>
      </div>
    </div>
  );
};

const Sidebar = (props) => {
  let {
    sideBar,
    cart,
    closeSideBar,
    productsCart,
    setFavorite,
    favorite,
    removeFromFavorite,
    addToFavorite,
  } = props;

  const versand = 0.0;

  const total = cart.reduce((result, item) => {
    return result + item.price;
  }, 0);

  let sum = versand + total;
  const getTax = sum * 0.19; //19% of tax

  if (!sideBar) {
    return (
      <div className="cart">
        <div className="dark-overflow" onClick={() => closeSideBar()} />

        <div className="sidebar ">
          <div className="row" onClick={() => closeSideBar()}>
            x
          </div>

          <div className="grip-container">
            <div className="checkout">
              <div className="container-cart">
                <div className="cart-top">
                  <div className="title">Warenkorb</div>

                  <div className="CartButton">
                    <div className="CartActions">
                      <div className="CartActionsButton">
                        <div>
                          <div id="amazonButton">
                            <img
                              className="amazonpay-button-inner-image"
                              style={{ cursor: "pointer", maxHeight: "45px" }}
                              alt="AmazonPay"
                              src="https://d23yuld0pofhhw.cloudfront.net/default/de/live/lwa/gold/medium/PwA.png"
                              tabIndex="0"
                              data-metric-tags="highres"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="actionOr"> – Oder –</div>
                      <div className="checkout">
                        <a
                          href="#"
                          className="checkout-Button"
                          data-inspect="CartBottomCheckout"
                          href="#"
                        >
                          Weiter zur Kasse
                        </a>
                      </div>
                    </div>
                  </div>

                  <div className="cart-products">{productsCart}</div>
                  <div className="action">
                    <div className="coupon">
                      Gutscheincode:
                      <input
                        className="input"
                        placeholder="  Code eintragen ..."
                      />
                    </div>
                    <div className="cardSummary">
                      <div className="totals">
                        <span className="span1">Zwischensumme € {total}</span>
                        <span className="span2">Versand € {versand}</span>
                        <span className="span3">Gesamtsumme € {total}</span>
                        <span className="span4">
                          inkl. 19% MwSt. € {getTax}{" "}
                        </span>
                      </div>
                    </div>
                    <div className="CartButton">
                      <div className="CartActions">
                        <div className="CartActionsButton">
                          <div>
                            <div id="amazonButton">
                              <img
                                className="amazonpay-button-inner-image"
                                style={{ cursor: "pointer", maxHeight: "45px" }}
                                alt="AmazonPay"
                                src="https://d23yuld0pofhhw.cloudfront.net/default/de/live/lwa/gold/medium/PwA.png"
                                tabIndex="0"
                                data-metric-tags="highres"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="actionOr"> – Oder –</div>
                        <div className="checkout">
                          <a
                            href="#"
                            className="checkout-Button"
                            data-inspect="CartBottomCheckout"
                            href="#"
                          >
                            Weiter zur Kasse
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="wishlist">
              <div className="container-cart">
                <div className="cart-top">
                  <div className="title">Mein Wunschzettel</div>
                  <div className="action"></div>
                </div>
                <Wishlist
                  favorite={favorite}
                  setFavorite={setFavorite}
                  addToFavorite={addToFavorite}
                  removeFromFavorite={removeFromFavorite}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return <div />;
  }
};

export default Cart;
