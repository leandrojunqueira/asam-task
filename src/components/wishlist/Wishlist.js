import React from "react";

const Wishlist = (props) => {
  let { favorite, removeFromFavorite } = props;

  //Map Products added to cart
  const wishListCart = favorite.map((i) => (
    <div key={i.id} className="">
      <div className="simple-Cart">
        <ul className="cart-list ">
          <li className="listItem">
            <div className="cartItem">
              <div className="cartItem-image">
                <img
                  src={i.productImage.retina}
                  alt={i.name}
                  className="item-image"
                />
              </div>
              <div className="itemInfo">
                <span className="brand itemName">{i.cosBrand}</span>

                <span className="itemName">{i.name}</span>
                <div className="itemDescription">
                  {i.cosContent} -<span> {i.normPrice}</span>
                </div>
                <div className="itemDescription">{i.shortDescription}</div>
                <div className="itemAction">
                  <span
                    className="Link LinkPurple"
                    aria-hidden="true"
                    onClick={() => removeFromFavorite(i)}
                  >
                    Entfernen
                  </span>
                  <span className="Divider DividerVertical"></span>
                </div>
              </div>
              <div className="qtd">
                <p>Anzahl</p>
                <div style={{ position: "relative", opacity: "1" }}>
                  <div className="qtdNumber">1</div>
                </div>
              </div>
              <p className="itemPrice textPrice">{i.price} €</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  ));

  return (
    <div>
      {wishListCart}

      <div className="CartButton">
        <div className="CartActions">
          <div className="checkout">
            <a
              href="#"
              className="checkout-Button"
              data-inspect="CartBottomCheckout"
              href="#"
            >
              Alles in den Warenkorb
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Wishlist;
