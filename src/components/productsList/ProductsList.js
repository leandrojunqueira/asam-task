import React from "react";
import Products from "./products.json";
import heart from "assets/images/wishlist.svg";

const ProductsList = (props) => {
  let { addToCart, addToFavorite } = props;

  const list = Products.products.map((i) => (
    <div className="card" key={i.id}>
      <picture className="thumbnail">
        <span
          className="favorite"
          value="add"
          type="submit"
          onClick={() => addToFavorite(i)}
        >
          <img src={heart} className="favoriteIcon" width={20} />
        </span>
        <img className="img" src={i.productImage.retina} alt="" />
      </picture>
      <span className="brand">{i.cosBrand}</span>
      <span className="name"> {i.name} </span>
      <span className="content">{i.cosContent}</span>
      <span className="price"> {i.price} €</span>
      <span className="norprice">{i.normPrice}</span>

      <div className="buttonAction">
        <button
          className="addToCart"
          value="add"
          type="submit"
          onClick={() => addToCart(i)}
        >
          In den Warenkorb
        </button>
      </div>
    </div>
  ));

  return <div className="cards">{list}</div>;
};
export default ProductsList;
