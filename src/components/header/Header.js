import React from "react";
import logo from "assets/images/logo.svg";
import logoMobile from "assets/images/logo-mobile.svg";
import Cart from "../cart/Cart";

const Header = (props) => {
  let {
    cart,
    setCart,
    remove,
    setFavorite,
    favorite,
    removeFromFavorite,
    addToFavorite,
  } = props;

  return (
    <div className="header">
      <img className="logo" src={logo} alt="Logo" />
      <img className="logo-mobile" src={logoMobile} alt="Logo Mobile" />

      <div className="menuRight">
        <Cart
          cart={cart}
          setCart={setCart}
          remove={remove}
          favorite={favorite}
          setFavorite={setFavorite}
          addToFavorite={addToFavorite}
          removeFromFavorite={removeFromFavorite}
        />
      </div>
    </div>
  );
};

export default Header;
