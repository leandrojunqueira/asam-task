import React from "react";
import ProductsList from "../productsList/ProductsList";

const Home = (props) => {
  let { cart, addToCart, remove, addToFavorite } = props;

  return (
    <div>
      <ProductsList
        addToFavorite={addToFavorite}
        cart={cart}
        addToCart={addToCart}
        remove={remove}
      />
    </div>
  );
};

export default Home;
