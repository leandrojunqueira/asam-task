import React, { useState } from "react";
import "./App.css";
import Header from "../components/header/Header";
import Home from "../components/page/Home";

function App() {
  const [cart, setCart] = useState([]);
  const [favorite, setFavorite] = useState([]);

  //add to cart
  const addToCart = (i) => {
    let addIt = true;
    for (let e = 0; e < cart.length; e++) {
      if (cart[e].id === i.id) addIt = false;
    }
    if (addIt) {
      setCart([...cart, i]);

      window.alert(
        "Product was added to the Warenkorb! Click in Warenkorb to see it."
      );
    } else {
      window.alert("Product already in the Warenkorb :)");
    }
  };
  //remove from cart
  const removeFromCart = (i) => {
    let hardCopy = [...cart];
    hardCopy = hardCopy.filter((cartItem) => cartItem.id !== i.id);
    setCart(hardCopy);
  };

  //add to wishlist
  const addToFavorite = (i) => {
    let addIt = true;
    for (let e = 0; e < favorite.length; e++) {
      if (favorite[e].id === i.id) addIt = false;
    }
    if (addIt) {
      setFavorite([...favorite, i]);

      window.alert("Product was added to the Wunschzettel :)");
    } else {
      window.alert("Product already in the Wunschzettel");
    }
  };
  //remove from wishlist
  const removeFromFavorite = (i) => {
    let add = [...favorite];
    add = add.filter((item) => item.id !== i.id);
    setFavorite(add);
  };

  return (
    <div className="App container">
      <Header
        cart={cart}
        setCart={setCart}
        remove={removeFromCart}
        favorite={favorite}
        setFavorite={setFavorite}
        addToFavorite={addToFavorite}
        removeFromFavorite={removeFromFavorite}
      />
      <Home
        addToCart={addToCart}
        favorite={favorite}
        setFavorite={setFavorite}
        addToFavorite={addToFavorite}
        removeFromFavorite={removeFromFavorite}
      />
    </div>
  );
}

export default App;
