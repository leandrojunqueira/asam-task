# Project shopping asambeauty.com

It's a simple responsive UX shopping cart by using React Hooks. You can add and remove products to the cart or wishlist.

demo: https://simple-shopping-cart-4svyx.ondigitalocean.app/

## Available Scripts

In the project directory, you can run:

### `npm run start`

### `npm run build`

### `npm test`

### `npm run eject`
